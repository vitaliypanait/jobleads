<?php

use Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces(['App' => dirname(dirname(__FILE__)) . '/src/App/']);
$loader->register();
