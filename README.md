To run project run next commands:

1. Install project environment
`docker-compose up -d --build`

2. Add `127.0.0.1 jobleads.local` into `/etc/hosts`

3. Mysql db structure
`cat config/mysqldump.sql | docker exec -i jl_mysql /usr/bin/mysql -uroot -proot jobleads`

4. Postgres db structure
`cat config/postgresdump.sql | docker exec -i jl_postgres psql -U root jobleads`

All statistics you can get from url: `http://jobleads.local/`

For using different databases change in `app/Providers/DatabaseServiceProvider.php` config from `mysql` to `postgres`