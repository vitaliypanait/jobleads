<?php

namespace PHPSTORM_META {

	override(
		\Phalcon\DiInterface::get(0),
		map([
			''              => '@',
			'modelsManager' => \Phalcon\Mvc\Model\Manager::class,
		])
	);
}