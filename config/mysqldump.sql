CREATE TABLE IF NOT EXISTS `states` (
  `id`    INT(10)     UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`  VARCHAR (255)        NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `counties` (
  `id`       INT(10)       UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`     VARCHAR(255)           NOT NULL,
  `state_id` INT(10)       UNSIGNED NOT NULL,
  `tax_rate` NUMERIC(5, 2) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT states_id_fkey FOREIGN KEY (state_id)
    REFERENCES states(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `tax_statistics` (
  `id`        INT(10)        UNSIGNED NOT NULL AUTO_INCREMENT,
  `state_id`  INT(10)        UNSIGNED NOT NULL,
  `county_id` INT(10)        UNSIGNED NOT NULL,
  `tax_rate`  NUMERIC(5, 2)  UNSIGNED NOT NULL DEFAULT 0,
  `tax`       NUMERIC(21, 2) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;