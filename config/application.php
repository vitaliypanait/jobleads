<?php

return [
    'database' => [
        'mysql' => [
            'host'     => 'mysql',
            'username' => 'root',
            'password' => 'root',
            'dbname'   => 'jobleads',
            'adapter'  => 'mysql',
        ],
        'postgres' => [
            'host'     => 'postgres',
            'username' => 'root',
            'password' => 'root',
            'dbname'   => 'jobleads',
            'adapter'  => 'postgresql',
        ],
    ],
    'application' => [
        'controllersDir' => __DIR__ . '/../app/Http/Controllers',
        'modelsDir'      => __DIR__ . '/../app/Models/',
        'viewsDir'       => __DIR__ . '/../resources/views/',
    ],
];
