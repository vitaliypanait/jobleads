CREATE TABLE IF NOT EXISTS states(
   id   serial PRIMARY KEY,
   name VARCHAR(256) NOT NULL
);

CREATE TABLE IF NOT EXISTS counties(
   id        serial PRIMARY KEY,
   name      VARCHAR(256)  NOT NULL,
   state_id  INTEGER       NOT NULL,
   tax_rate  NUMERIC(5, 2) NOT NULL DEFAULT 0,
   CONSTRAINT states_id_fkey FOREIGN KEY (state_id)
      REFERENCES states (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS tax_statistics(
   id        serial PRIMARY KEY,
   state_id  INTEGER        NOT NULL,
   county_id INTEGER        NOT NULL,
   tax_rate  NUMERIC(5, 2)  NOT NULL DEFAULT 0,
   tax       NUMERIC(21, 2) NOT NULL DEFAULT 0
);