<?php

use App\Providers;

return [
    Providers\ConfigServiceProvider::class,
    Providers\DatabaseServiceProvider::class,
    Providers\ModelsMetadataServiceProvider::class,
    Providers\MvcDispatcherServiceProvider::class,
    Providers\ViewServiceProvider::class,
    Providers\RouterServiceProvider::class,
    Providers\ResponseServiceProvider::class,
    Providers\ModelsManagerServiceProvider::class,

    \App\Services\Statistics\StatisticsServiceProvider::class,
];
