<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class TaxStatistics extends Model
{

    /** @var int */
    public $state_id;

    /** @var int */
    public $county_id;

    /** @var int */
    public $tax_rate;

    /** @var int */
    public $tax;

}