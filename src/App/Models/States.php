<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class States extends Model
{

    /** @var int */
    public $id;

    /** @var string */
    public $name;

}