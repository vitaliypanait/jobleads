<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class Counties extends Model
{

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $state_id;

    /** @var int */
    public $tax_rate;

}