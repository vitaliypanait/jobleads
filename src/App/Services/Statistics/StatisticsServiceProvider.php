<?php

namespace App\Services\Statistics;

use App\Contracts\Statistics\Statistics as StatisticsContract;
use App\Providers\AbstractServiceProvider;

/**
 * Class StatisticsServiceProvider
 *
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 * @package App\Service\Statistics
 */
class StatisticsServiceProvider extends AbstractServiceProvider
{

    /** @var string */
    protected $serviceName = StatisticsContract::class;

    public function register(): void
    {
        $di = $this->di;

        $di->setShared($this->serviceName, function () use ($di) {
            return new Statistics($di->get('modelsManager'));
        });
    }
}
