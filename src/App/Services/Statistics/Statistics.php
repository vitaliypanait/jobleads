<?php

namespace App\Services\Statistics;

use App\Contracts\Statistics\Statistics as StatisticsContract;
use App\Models\Counties;
use App\Models\States;
use App\Models\TaxStatistics;
use Phalcon\Mvc\Model\Manager;

class Statistics implements StatisticsContract
{

    /**
     * @var \Phalcon\Mvc\Model\Manager
     */
    private $modelsManager;

    public function __construct(Manager $modelsManager)
    {
        $this->modelsManager = $modelsManager;
    }

    public function calculateOverallStatesTaxes(): array
    {
        $result = $this->modelsManager->createBuilder()
            ->from(['s' => States::class])
            ->columns('SUM(ts.tax) as summary, s.name')
            ->leftJoin(TaxStatistics::class, 's.id = ts.state_id', 'ts')
            ->groupBy('s.id')
            ->getQuery()
            ->execute();

        return array_column($result->toArray(), 'summary', 'name');
    }

    public function calculateAverageStatesTaxes(): array
    {
        $result = $this->modelsManager->createBuilder()
            ->from(['s' => States::class])
            ->columns('AVG(ts.tax) as average, s.name')
            ->leftJoin(TaxStatistics::class, 's.id = ts.state_id', 'ts')
            ->groupBy('s.id')
            ->getQuery()
            ->execute();

        return array_column($result->toArray(), 'average', 'name');
    }

    public function calculateAverageStatesTaxRate(): array
    {
        $result = $this->modelsManager->createBuilder()
            ->from(['s' => States::class])
            ->columns('AVG(c.tax_rate) as average, s.name')
            ->leftJoin(Counties::class, 's.id = c.state_id', 'c')
            ->groupBy('s.id')
            ->getQuery()
            ->execute();

        return array_column($result->toArray(), 'average', 'name');
    }

    public function calculateActualAverageTaxRate(): float
    {
        return (float) Counties::average(['column' => 'tax_rate']);
    }

    public function calculateAverageTaxRate(): float
    {
        return (float) TaxStatistics::average(['column' => 'tax_rate']);
    }

    public function calculateTotalTax(): float
    {
        return (float) TaxStatistics::sum(['column' => 'tax']);
    }
}