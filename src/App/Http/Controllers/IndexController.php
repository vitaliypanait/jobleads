<?php

namespace App\Http\Controllers;

use App\Contracts\Statistics\Statistics;
use Phalcon\Mvc\Controller;

class IndexController extends Controller
{

    public function indexAction()
    {
        $statistics = $this->di->get(Statistics::class);

        $this->view->setVars(
            [
                'overallStatesTaxes'   => $statistics->calculateOverallStatesTaxes(),
                'averageStatesTaxes'   => $statistics->calculateAverageStatesTaxes(),
                'averageStatesTaxRate' => $statistics->calculateAverageStatesTaxRate(),
                'averageTaxRate'       => $statistics->calculateAverageTaxRate(),
                'totalTax'             => $statistics->calculateTotalTax(),
            ]
        );
    }
}
