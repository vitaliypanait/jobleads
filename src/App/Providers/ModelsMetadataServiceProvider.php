<?php

namespace App\Providers;

use Phalcon\Mvc\Model\Metadata\Memory;

/**
 * \App\Providers\ModelsMetadataServiceProvider
 *
 * @package App\Providers
 */
class ModelsMetadataServiceProvider extends AbstractServiceProvider
{

    /** @var string */
    protected $serviceName = 'modelsMetadata';

    public function register(): void
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $metadata = new Memory();

                return $metadata;
            }
        );
    }
}
