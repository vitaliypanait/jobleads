<?php

namespace App\Providers;

use Phalcon\Db\Adapter\Pdo\Factory;

/**
 * Class DatabaseServiceProvider
 *
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 * @package App\Providers
 */
class DatabaseServiceProvider extends AbstractServiceProvider
{

    /**
     * @var string
     */
    protected $serviceName = 'db';

    public function register(): void
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                /** @var \Phalcon\DiInterface $this */
                $connection = Factory::load($this->getShared('config')->database->postgres->toArray());

                return $connection;
            }
        );
    }
}
