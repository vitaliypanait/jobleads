<?php

namespace App\Providers;

use Phalcon\Di\InjectionAwareInterface;

/**
 * @package App\Providers
 */
interface ServiceProviderInterface extends InjectionAwareInterface
{

    /**
     * Register application service.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     */
    public function register(): void;

    /**
     * Return service name.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return string
     */
    public function getName(): string;
}
