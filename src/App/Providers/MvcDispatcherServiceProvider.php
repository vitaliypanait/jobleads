<?php

namespace App\Providers;

use Phalcon\Mvc\Dispatcher;

/**
 * Class MvcDispatcherServiceProvider
 *
 * @package App\Providers
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 */
class MvcDispatcherServiceProvider extends AbstractServiceProvider
{

    /** @var string */
    protected $serviceName = 'dispatcher';

    /** @var string */
    protected $namespace = 'App\Http\Controllers';

    public function register(): void
    {
        $namespace = $this->namespace;

        $this->di->setShared(
            $this->serviceName,
            function () use ($namespace) {
                $dispatcher = new Dispatcher();
                $dispatcher->setDefaultNamespace($namespace);

                return $dispatcher;
            }
        );
    }
}
