<?php

namespace App\Providers;

use Phalcon\DiInterface;
use Phalcon\Mvc\User\Component;

/**
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 * @package App\Providers
 */
abstract class AbstractServiceProvider extends Component implements ServiceProviderInterface
{

    /** @var string */
    protected $serviceName;

    /**
     * @param  DiInterface  $di
     */
    public function __construct(DiInterface $di)
    {
        $this->setDI($di);
    }

    public function getName(): string
    {
        return $this->serviceName;
    }
}
