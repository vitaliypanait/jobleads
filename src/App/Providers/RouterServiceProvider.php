<?php

namespace App\Providers;

/**
 * Class RouterServiceProvider
 *
 * @package App\Providers
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 */
class RouterServiceProvider extends AbstractServiceProvider
{

    /** @var string */
    protected $serviceName = 'router';

    public function register(): void
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                /** @var \Phalcon\DiInterface $this */
                $appPath = $this->getShared('bootstrap')->getApplicationPath();

                return require $appPath . '/src/App/Http/routes.php';
            }
        );
    }
}
