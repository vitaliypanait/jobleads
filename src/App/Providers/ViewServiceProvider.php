<?php

namespace App\Providers;

use Phalcon\Mvc\View;

/**
 * Class ViewServiceProvider
 *
 * @package App\Providers
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 */
class ViewServiceProvider extends AbstractServiceProvider
{

    /** @var string */
    protected $serviceName = 'view';

    public function register(): void
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                /** @var \Phalcon\DiInterface $this */
                $config = $this->getShared('config')->application;

                $view = new View();

                $view->setViewsDir($config->viewsDir);

                return $view;
            }
        );
    }
}
