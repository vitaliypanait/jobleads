<?php

namespace App\Providers;

use Phalcon\Http\Response;

/**
 * Class ResponseServiceProvider
 *
 * @package App\Providers
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 */
class ResponseServiceProvider extends AbstractServiceProvider
{
    /**
     * The Service name.
     * @var string
     */
    protected $serviceName = 'response';

    /**
     * Register application service.
     *
     * @return void
     */
    public function register(): void
    {
        $this->di->setShared($this->serviceName, Response::class);
    }
}
