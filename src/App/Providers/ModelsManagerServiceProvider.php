<?php

namespace App\Providers;

use Phalcon\Mvc\Model\Manager;

/**
 * Class ModelsManagerServiceProvider
 *
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 * @package App\Providers
 */
class ModelsManagerServiceProvider extends AbstractServiceProvider
{

    /** @var string  */
    protected $serviceName = 'modelsManager';

    public function register(): void
    {
        $this->di->setShared($this->serviceName, Manager::class);
    }
}
