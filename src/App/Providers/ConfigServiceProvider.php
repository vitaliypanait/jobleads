<?php

namespace App\Providers;

use Phalcon\Config;

/**
 * Class ConfigServiceProvider
 *
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 * @package App\Providers
 */
class ConfigServiceProvider extends AbstractServiceProvider
{

    /** @var string */
    protected $serviceName = 'config';

    public function register(): void
    {
        $this->di->setShared(
            $this->serviceName,
            function () {
                $config = [];
                /** @var \Phalcon\DiInterface $this */
                $appPath = $this->getShared('bootstrap')->getApplicationPath();

                if (file_exists($appPath . '/config/application.php')) {
                    /** @noinspection PhpIncludeInspection */
                    $config = include $appPath . '/config/application.php';
                }

                if (is_array($config)) {
                    $config = new Config($config);
                }

                return $config;
            }
        );
    }
}
