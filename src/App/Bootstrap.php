<?php

namespace App;

use InvalidArgumentException;
use Phalcon\Di;
use Phalcon\DiInterface;
use Phalcon\Mvc\Application;
use App\Providers\ServiceProviderInterface;

/**
 * @package App
 * @author  Vitaliy Panait <panait.v@yandex.ru>
 */
class Bootstrap
{

    /** @var DiInterface */
    protected $di;

    /** @var string */
    protected $applicationPath;

    /** @var ServiceProviderInterface[] */
    protected $serviceProviders = [];

    /** @var \Phalcon\Mvc\Application */
    protected $app;

    /**
     * @param $applicationPath
     */
    public function __construct($applicationPath)
    {
        if (! is_dir($applicationPath)) {
            throw new InvalidArgumentException("The $applicationPath must be a valid application path");
        }

        $this->di              = new Di();
        $this->applicationPath = $applicationPath;

        $this->di->setShared('bootstrap', $this);
        Di::setDefault($this->di);

        $providers = include $applicationPath . '/config/providers.php';
        if (is_array($providers)) {
            $this->initializeServices($providers);
        }

        $this->app = new Application;
        $this->app->setDI($this->di);
    }

    /**
     * Gets the Dependency Injector.
     *
     * @return Di
     */
    public function getDi()
    {
        return $this->di;
    }

    /**
     * Get application path.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return string
     */
    public function getApplicationPath()
    {
        return $this->applicationPath;
    }

    /**
     * Run application
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return bool|\Phalcon\Http\ResponseInterface|string
     */
    public function run()
    {
        return $this->getOutput();
    }

    /**
     * Get application output.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return bool|\Phalcon\Http\ResponseInterface|string
     */
    protected function getOutput()
    {
        if ($this->app instanceof Application) {
            $response = $this->app->handle();

            return $response->getContent();
        }

        return $this->app->handle();
    }

    /**
     * Initialize Services in the Dependency Injector Container.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @param  array  $providers
     */
    protected function initializeServices(array $providers)
    {
        foreach ($providers as $name => $class) {
            $this->initializeService(new $class($this->di));
        }
    }

    /**
     * Initialize the Service in the Dependency Injector Container.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @param  \App\Providers\ServiceProviderInterface  $serviceProvider
     *
     * @return $this
     */
    protected function initializeService(ServiceProviderInterface $serviceProvider)
    {
        $serviceProvider->register();
        $this->serviceProviders[$serviceProvider->getName()] = $serviceProvider;

        return $this;
    }
}
