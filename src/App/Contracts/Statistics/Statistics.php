<?php

namespace App\Contracts\Statistics;

interface Statistics
{

    /**
     * Output the overall amount of taxes collected per state.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return array
     */
    public function calculateOverallStatesTaxes(): array;

    /**
     * Output the average amount of taxes collected per state.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return array
     */
    public function calculateAverageStatesTaxes(): array;

    /**
     * Output the average county tax rate per state.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return array
     */
    public function calculateAverageStatesTaxRate(): array;

    /**
     * Output the actual average tax rate of the country.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return float
     */
    public function calculateActualAverageTaxRate(): float;

    /**
     * Output the average tax rate of the country.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return float
     */
    public function calculateAverageTaxRate(): float;

    /**
     * Output the collected overall taxes of the country.
     *
     * @author Vitaliy Panait <panait.v@yandex.ru>
     *
     * @return float
     */
    public function calculateTotalTax(): float;
}